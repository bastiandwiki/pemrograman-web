<?php
class matematika{
	public $pi = 3.14;
	function tambah ($a,$b){
		$c = $a + $b;
		return $c;
	}
	function kuadrat ($x){
		return $x * $x;
	}
	function keliling_lingkaran($jari){
		$kel = 2 * $this->pi * $jari;
	return $kel;
}
	function luas_lingkaran($jari){
		$luas = $this->pi * $this->kuadrat($jari);
		return $luas;
	}
}

$math= new matematika();
$jari = 10;
$kel_lingkaran = $math->keliling_lingkaran($jari);
$luas_lingkaran = $math->luas_lingkaran($jari);

echo "Menghitung Keliling Dan Luas Lingkaran<br>";
echo "Jari-Jari : ".$jari."<br>";
echo "Keliling = ".$kel_lingkaran."<br>";
echo " Luas = " .$luas_lingkaran;
?>